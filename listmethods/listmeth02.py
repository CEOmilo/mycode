#!/usr/bin/env python3

# My favorite wow classes in a dope python list
best_classes = ["evoker", "warrior", "druid", "monk" ]
# 'Popping' the last item of the list i.e. removing it
best_classes.pop()
print(best_classes)
# Asking for input here, although it won't be stored anywhere
input("Can you guess which of my favorite classes are missing?: ")

