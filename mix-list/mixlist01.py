#!/usr/bin/env python3

# A list with the IP, Port number, and state
my_list = [ "192.168.0.5", 5060, "UP" ]

# Storing IP in the first entry
print("The first item in the list (IP): " + my_list[0] )
# Converting an integer to a string for the port number
print("The second item in the list (port): " + str(my_list[1]) )
# Storing the state as the final entry
print("The last item in the list (state): " + my_list[2] )
